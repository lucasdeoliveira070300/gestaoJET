import { Router } from 'express';
import SessionController from './app/controllers/SessionController';
import UserController from './app/controllers/Usercontroller';
import authMiddleware from './app/middlewares/auth';
import adminMiddleware from './app/middlewares/admin'

const Routes = new Router();
routes.post('/User', UserController.store);
Routes.post('/Session', SessionController.session);
Routes.use(authMiddleware);
routes.get('/User', adminMiddleware, UserController.index);
routes.get('/User/:id_user', adminMiddleware, UserController.details);
Routes.get('/test', (req, res) => res.json({message: 'Its OK!'}));

export default Routes;