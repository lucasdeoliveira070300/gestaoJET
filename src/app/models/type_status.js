import Sequelize, { Model } from 'sequelize';
import bcrypt from 'bcryptjs';

class Type_status extends Model {

    static init(sequelize){
        super.init(
        {
            id_type_status: { 
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            description: Sequelize.STRING,
            },
            {
            sequelize,
            freezeTableName: 'Type_status',
            tableName: 'Type_status'
            }
        );
        return this;
    }
}
export default Type_status;