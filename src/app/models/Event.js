import Sequelize, { Model } from 'sequelize';
import bcrypt from 'bcryptjs';

class Event extends Model {

    static init(sequelize){
        super.init(
        {
            id_event: { 
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            id_user: Sequelize.INTEGER,
            id_type: Sequelize.INTEGER,
            subject: Sequelize.STRING,
            description: Sequelize.STRING,
            date: Sequelize.STRING,
            hour: Sequelize.STRING,
            local: Sequelize.STRING,
            },
            {
            sequelize,
            freezeTableName: 'Event',
            tableName: 'Event'
            }
        );
        return this;
    }

    static associate(models){
        this.belongsTo(models.User, {foreignKey: 'id_user', as: 'user'});
        this.belongsTo(models.Type_evt_pub, {foreignKey: 'id_evt_pub', as: 'type_evt_pub'});
    }
}
export default Event;