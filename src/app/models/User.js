import Sequelize, { Model } from 'sequelize';
import bcrypt from 'bcryptjs';

class User extends Model {

    static init(sequelize){
        super.init(
        {
            id_user: { 
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            id_user_type: Sequelize.INTEGER,
            name: Sequelize.STRING,
            date_birth: Sequelize.STRING,
            email: Sequelize.STRING,
            telefone: Sequelize.STRING,
            username: Sequelize.STRING,
            password: Sequelize.STRING,
            password_virtual: Sequelize.VIRTUAL,
            },
            {
            sequelize,
            freezeTableName: 'User',
            tableName: 'User'
            }
        );
        this.addHook('beforeSave', async user => {
            if(user.password_virtual){
                user.password = await bcrypt.hash(user.password, 8);
            }
        });
        return this;
    }

    static associate(models){
        this.belongsTo(models.User_type, {foreignKey: 'id_user_type', as: 'user_type'});
    }

    checkPassword(password_virtual){
        return bcrypt.compare(password_virtual, this.password);
    }

}
export default User;