import {op} from 'sequelize';
import User from '../models/User';

export default async (req, res, next) => {
    const isAdmin = await User.findOne({
        where: {
            [op.and]:{
                id_user: req.req.idUser,
                id_user_type: '1'
            }
        }
    });
    if(!isAdmin){
        return res.status(401).json({ error: 'Only admin can acess this page!' });
    }
    return next();
};