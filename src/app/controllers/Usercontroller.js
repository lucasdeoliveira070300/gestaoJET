import * as Yup from 'yup';
import {op} from 'sequelize';
import User from '../models/User';


class UserController{
    async store(req, res){
        const schema = Yup.object().shape({
            id_user_type: Yup.number().required(),
            name: Yup.string().required().max(50),
            date_birth: Yup.date().required(),
            email: Yup.string().required().max(50),
            telefone: Yup.string().required().max(30),
            username: Yup.string().required().max(20),
            password_virtual: Yup.string().required().min(6).max(12),
        });
        if(!(await schema.isValid(req.body))){
            return res.status(400).json({error: 'Validation fails!'});
        }
        const UserExists = await User.findOne({
            attributes: ['id_user'],
            where: {
                [op.or]:{
                    cpf: req.body.cpf,
                    email: req.body.email 
                }
            }
        });

        if(UserExists){
            return res.status(400).json({error: 'User already exists!'});
        }

        const {id_user,name, email} = await User.create(req.body);
        return res.json({
            id_user, name, email
        });     
    }
    async index(req,res){
        const {page = 1} = req.query;
        const user = await User.findAll({
            where: {'id_user_type': !'1' },
            limit: 10,
            offset: (page -1) * 10,
            order: ['name'],
            attributes: [
                'id_user', 'name','email','cpf','telefone','date_birth'
            ]
        });
        return res.json(user);
    }
    async details(req,res){
        const user = await User.findOne({
            where: {id_user: req.params.id_user},
            attributes: [
                'id_user','name','email','cpf','telefone','date_birth'
            ]
        });
        if(!user){
            return res.status(400).json({error: 'User do not exists!'});
        }
        return res.json(user);
    }
}
export default UserController;  