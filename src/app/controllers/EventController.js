import * as Yup from 'yup';
import {op} from 'sequelize';
import User from '../models/User';

class EventController{
    async store(req, res){
        const schema = Yup.object().shape({
            id_user: Yup.number().required(),
            id_type: Yup.number().required(),
            subject: Yup.string().required().max(100),
            description: Yup.string().required().max(255),
            date: Yup.date(),
            hour: Yup.string().max(5),
            local: Yup.string().max(100),
        });
        if(!(await schema.isValid(req.body))){
            return res.status(400).json({error: 'Validation fails!'});
        }
        
        if( schema.date < '07/05/2020'){
            return res.status(400).json({error: 'Dates in past is not valid!'});
        }
    }
}