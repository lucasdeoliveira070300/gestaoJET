const bcrypt = require('bcryptjs');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.bulkInsert('User', [
      { 
        id_user_type: '1',
        name: 'Admin',
        date_birth: '07/03/2000',
        email: 'admin@test.com',
        telefone:'5599999999',
        username: 'admin',
        // password: bcrypt.hashSync('admin', 0),
        password: 'admin123',
        created_at: new Date(),
        updated_at: new Date()
      }
    ],
    {
      
    }
    );
  },

  down: () => {
  
  }
};
