module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('User',{
      id_user:{
        type: Sequelize.INTEGER, 
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      id_user_type:{
        type: Sequelize.INTEGER, 
        allowNull: false,
        references: {model: 'User_type', key: 'id_user_type'},
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      },
      name:{
        type: Sequelize.STRING(50),
        allowNull: false,
      },
      date_birth:{
        type: Sequelize.DATEONLY,
        allowNull: false,
      },
      email:{
        type: Sequelize.STRING(50),
        allowNull: false,
        unique: true
      },
      telefone:{
        type: Sequelize.STRING(30),
        allowNull: false
      },
      username:{
        type: Sequelize.STRING(20),
        allowNull: false,
        unique: true
      },
      password:{
        type: Sequelize.STRING(20),
        allowNull: false,
      },
      created_at:{
        type: Sequelize.DATE,
        allowNull: false
      },
      updated_at:{
        type: Sequelize.DATE,
        allowNull: false
      }

    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('User');
  }
};