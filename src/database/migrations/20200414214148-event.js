module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Event',{
      id_event:{
        type: Sequelize.INTEGER, 
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      id_user:{
        type: Sequelize.INTEGER, 
        allowNull: false,
        references: {model: 'User', key: 'id_user'},
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      },
      id_type:{
        type: Sequelize.INTEGER, 
        allowNull: false,
        references: {model: 'Type_evt_pub', key: 'id_evt_pub'},
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      },
      subject:{
        type: Sequelize.STRING(100),
        allowNull: false,
      },
      description:{
        type: Sequelize.STRING(255),
        allowNull: false,
      },
      date:{
        type: Sequelize.DATEONLY,
        allowNull: false,
      },
      hour:{
        type: Sequelize.STRING(5),
        allowNull: false
      },
      local:{
        type: Sequelize.STRING(100),
        allowNull: false,
      },
      created_at:{
        type: Sequelize.DATE,
        allowNull: false
      },
      updated_at:{
        type: Sequelize.DATE,
        allowNull: false
      }

    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Event');
  }
};