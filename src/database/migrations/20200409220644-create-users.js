module.exports = {
  up: (queryInterface, Sequelize) => {
  
    return queryInterface.createTable('User_type',{
      id_user_type:{
        type: Sequelize.INTEGER, 
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      description:{
        type: Sequelize.STRING(50),
        allowNull: false
      },
      created_at:{
        type: Sequelize.DATE,
        allowNull: false
      },
      updated_at:{
        type: Sequelize.DATE,
        allowNull: false
      }

    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('User_type');
  }
};
